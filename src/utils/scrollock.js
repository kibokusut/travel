export function disableScroll() {
  // Simpan posisi scroll saat ini
  const scrollY = window.scrollY;
  const body = document.body;
  const html = document.documentElement;

  // Tetapkan kelas CSS untuk mengunci scroll
  body.style.position = "fixed";
  body.style.top = `-${scrollY}px`;
  body.style.width = "100%";

  // Mengunci elemen html juga untuk cross-browser support
  html.style.overflow = "hidden";
}

export function enableScroll() {
  const body = document.body;
  const html = document.documentElement;

  // Menghapus kelas CSS dan mengembalikan scroll
  const scrollY = parseInt(body.style.top || "0", 10);
  body.style.position = "";
  body.style.top = "";
  body.style.width = "";
  html.style.overflow = "";

  // Menggulir kembali ke posisi sebelumnya
  window.scrollTo(0, scrollY * -1);
}
