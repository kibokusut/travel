"use client";
import { useState } from "react";
import { IoIosArrowDown, IoIosArrowUp } from "react-icons/io";
const Dropdownfilter = () => {
  const [show, setShow] = useState(false);
  const handleShow = () => {
    setShow(!show);
  };
  return (
    <ul
      style={{
        listStyle: "none",
        background: "white",
        padding: 12,
        borderRadius: 4,
        marginBottom: 12,
      }}
    >
      <div
        onClick={handleShow}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          cursor: "pointer",
        }}
      >
        <p style={{ fontWeight: "bold" }}>name</p>
        {show ? <IoIosArrowUp /> : <IoIosArrowDown />}
      </div>
      {show && (
        <div style={{ marginTop: 12 }}>
          <li style={{ marginBottom: 6 }}>
            <input style={{ marginRight: 6 }} type="checkbox" />
            <label htmlFor="">filter satu</label>
          </li>
          <li style={{ marginBottom: 6 }}>
            <input style={{ marginRight: 6 }} type="checkbox" />
            <label htmlFor="">filter satu</label>
          </li>
        </div>
      )}
    </ul>
  );
};

export default Dropdownfilter;
