"use client";
import styles from "./page.module.css";
import Header from "./component/header/header";
import Tourtype from "./component/home/tourtype";
import TourPackage from "./component/home/tourPackages";
import React from "react";
import TravelReview from "./component/home/travelReview";
import Destination from "./component/home/destination";

export default function Home() {
  return (
    <main className={styles.main}>
      <Header />
      <Tourtype />
      <TourPackage />
      <Destination />
      <TravelReview />
    </main>
  );
}
