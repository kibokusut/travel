"use client";
import Slider from "react-slick";
import Image from "next/image";
import {
  IoIosArrowDroprightCircle,
  IoIosArrowDropleftCircle,
} from "react-icons/io";
import { MdStar } from "react-icons/md";

const dataReview = [
  {
    name: "nama 1",
  },
  { name: "nama 2" },
  { name: "nama 3" },
  { name: "nama 4" },
  { name: "nama 5" },
  { name: "nama 6" },
  { name: "nama 7" },
  { name: "nama 8" },
];

const ArrowLeft = ({ onClick }) => {
  return (
    <IoIosArrowDropleftCircle
      size={50}
      color="FF6B00"
      onClick={onClick}
      style={{
        position: "absolute",
        zIndex: 1,
        top: "40%",
        bottom: 0,
        left: "-24px",
        transform: "translateX(-50%)",
        cursor: "pointer",
      }}
    />
  );
};
const ArrowRight = ({ onClick }) => {
  return (
    <IoIosArrowDroprightCircle
      size={50}
      color="FF6B00"
      onClick={onClick}
      style={{
        position: "absolute",
        zIndex: 1,
        top: "40%",
        bottom: 0,
        right: "-50px",
        cursor: "pointer",
      }}
    />
  );
};
const TravelReview = () => {
  var settings = {
    dots: true,
    infinite: true,
    speed: 800,
    slidesToShow: 3,
    slidesToScroll: 1,
    nextArrow: <ArrowRight />,
    prevArrow: <ArrowLeft />,
  };
  return (
    <div
      style={{
        margin: "0 auto",
        width: "85%",
        marginTop: 72,
        marginBottom: 72,
      }}
    >
      <h2 style={{ marginBottom: 12 }}>TRAVEL REVIEW</h2>
      <Slider {...settings}>
        {dataReview.map((d, i) => {
          return (
            <div key={i}>
              <div
                style={{
                  backgroundColor: "white ",
                  padding: 16,
                  margin: 10,
                  borderRadius: 12,
                }}
              >
                <div style={{ display: "flex", marginBottom: 12 }}>
                  <div
                    style={{
                      height: 40,
                      width: 40,
                      borderRadius: 40,
                      position: "relative",
                      marginRight: 12,
                    }}
                  >
                    <Image
                      style={{
                        borderRadius: 40,
                        objectFit: "cover",
                      }}
                      src={"/images/sample2.png"}
                      alt="My Image"
                      fill
                      sizes="400px"
                      //   objectFit="cover"
                    />
                  </div>
                  <div>
                    <p>{d.name}</p>
                    <span>
                      <MdStar color="#FFAF12" />
                      <MdStar color="#FFAF12" />
                      <MdStar color="#FFAF12" />
                      <MdStar color="#FFAF12" />
                      <MdStar color="#FFAF12" />
                    </span>
                  </div>
                </div>
                <div
                  style={{
                    background: "white",
                    fontSize: 13,
                    fontWeight: "normal",
                  }}
                >
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Ullam, quae et voluptas debitis eos eum architecto blanditiis
                  tempora perferendis fuga officiis repudiandae consectetur
                  beatae corrupti sint explicabo deleniti amet quidem.
                </div>
              </div>
            </div>
          );
        })}
      </Slider>
    </div>
  );
};

export default TravelReview;
