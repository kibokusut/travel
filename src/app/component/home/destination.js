"use client";
import Image from "next/image";

const Destination = () => {
  return (
    <div
      style={{
        marginTop: 125,
        textAlign: "left",
        backgroundImage: "url('/images/destination.png')",
        backgroundSize: "cover",
        backgroundPosition: "center",
        padding: "70px 24px",
        position: "relative",
        color: "white",
      }}
    >
      <div
        style={{
          height: "100%",
          width: "100%",
          position: "absolute",
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          background: "#00000047",
        }}
      ></div>
      <div
        style={{
          margin: "0 auto",
          width: "85%",
          zIndex: 1,
          //   position: "absolute",
          position: "relative",
        }}
      >
        <p style={{ marginBottom: 8 }}>Find a Tour By</p>
        <h2 style={{ marginBottom: 32 }}>DESTINATION</h2>

        <div
          style={{
            display: "grid",
            gridTemplateColumns: "repeat(auto-fill, minmax(300px, 2fr))",
            gridGap: 16,
          }}
        >
          <div
            style={{
              textAlign: "center",
              margin: "0 6px",
              position: "relative",
              height: 115,
              cursor: "pointer",
            }}
          >
            <div
              style={{
                position: "absolute",
                bottom: 0,
                left: 0,
                right: 0,
                background:
                  "linear-gradient(rgb(255 255 255 / 0%), rgb(81 81 81))",
                zIndex: 1,
                color: "white",
                padding: "12px 0px",
                borderBottomLeftRadius: 12,
                borderBottomRightRadius: 12,
              }}
            >
              <p>label</p>
            </div>
            <Image
              style={{ borderRadius: 12, objectFit: "cover" }}
              src="/images/sample1.png"
              alt="My Image"
              fill
              sizes="400px"
            />
          </div>
          <div
            style={{
              textAlign: "center",
              margin: "0 6px",
              position: "relative",
              height: 115,
              cursor: "pointer",
            }}
          >
            <div
              style={{
                position: "absolute",
                bottom: 0,
                left: 0,
                right: 0,
                background:
                  "linear-gradient(rgb(255 255 255 / 0%), rgb(81 81 81))",
                zIndex: 1,
                color: "white",
                padding: "12px 0px",
                borderRadius: 12,
              }}
            >
              <p>label</p>
            </div>
            <Image
              style={{ borderRadius: 12, objectFit: "cover" }}
              src="/images/sample1.png"
              alt="My Image"
              fill
              sizes="400px"
            />
          </div>
          <div
            style={{
              textAlign: "center",
              backgroundColor: "aliceblue",
              margin: "0 6px",
              position: "relative",
              height: 115,
              cursor: "pointer",
              borderRadius: 12,
            }}
          >
            <div
              style={{
                position: "absolute",
                bottom: 0,
                left: 0,
                right: 0,
                background:
                  "linear-gradient(rgb(255 255 255 / 0%), rgb(81 81 81))",
                zIndex: 1,
                color: "white",
                padding: "12px 0px",
                borderRadius: 12,
              }}
            >
              <p>label</p>
            </div>
            <Image
              style={{ borderRadius: 12, objectFit: "cover" }}
              src="/images/sample1.png"
              alt="My Image"
              fill
              sizes="400px"
              // objectFit="cover"
            />
          </div>
          <div
            style={{
              textAlign: "center",
              backgroundColor: "aliceblue",
              margin: "0 6px",
              position: "relative",
              height: 115,
              cursor: "pointer",
              overflow: "hidden",
              borderRadius: 12,
            }}
          >
            <div
              style={{
                position: "absolute",
                bottom: 0,
                left: 0,
                right: 0,
                background:
                  "linear-gradient(rgb(255 255 255 / 0%), rgb(81 81 81))",
                zIndex: 1,
                color: "white",
                padding: "12px 0px",
                borderRadius: 12,
              }}
            >
              <p>label</p>
            </div>
            <Image
              style={{ borderRadius: 12, objectFit: "cover" }}
              src="/images/sample1.png"
              alt="My Image"
              fill
              sizes="400px"
              // objectFit="cover"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Destination;
