import Image from "next/image";

const Tourtype = () => {
  return (
    <div style={{ margin: "0 auto", width: "85%", marginTop: 72 }}>
      <h2 style={{ marginBottom: 16 }}>TOUR TYPE</h2>

      <div
        style={{
          display: "grid",
          gridTemplateColumns: "auto auto auto auto",
        }}
      >
        <div
          style={{
            textAlign: "center",
            margin: "0 6px",
            position: "relative",
            height: 115,
            cursor: "pointer",
          }}
        >
          <div
            style={{
              position: "absolute",
              bottom: 0,
              left: 0,
              right: 0,
              background:
                "linear-gradient(rgb(255 255 255 / 0%), rgb(81 81 81))",
              zIndex: 1,
              color: "white",
              padding: "12px 0px",
              borderBottomLeftRadius: 12,
              borderBottomRightRadius: 12,
            }}
          >
            <p>label</p>
          </div>
          <Image
            style={{ borderRadius: 12, objectFit: "cover" }}
            src="/images/sample1.png"
            alt="My Image"
            fill
            sizes="400px"
            // objectFit="cover"
          />
        </div>
        <div
          style={{
            textAlign: "center",
            margin: "0 6px",
            position: "relative",
            height: 115,
            cursor: "pointer",
          }}
        >
          <div
            style={{
              position: "absolute",
              bottom: 0,
              left: 0,
              right: 0,
              background:
                "linear-gradient(rgb(255 255 255 / 0%), rgb(81 81 81))",
              zIndex: 1,
              color: "white",
              padding: "12px 0px",
              borderBottomLeftRadius: 12,
              borderBottomRightRadius: 12,
            }}
          >
            <p>label</p>
          </div>
          <Image
            style={{ borderRadius: 12, objectFit: "cover" }}
            src="/images/sample1.png"
            alt="My Image"
            fill
            sizes="400px"
            // objectFit="cover"
          />
        </div>
        <div
          style={{
            textAlign: "center",
            backgroundColor: "aliceblue",
            margin: "0 6px",
            position: "relative",
            height: 115,
            cursor: "pointer",
          }}
        >
          <div
            style={{
              position: "absolute",
              bottom: 0,
              left: 0,
              right: 0,
              background:
                "linear-gradient(rgb(255 255 255 / 0%), rgb(81 81 81))",
              zIndex: 1,
              color: "white",
              padding: "12px 0px",
              borderBottomLeftRadius: 12,
              borderBottomRightRadius: 12,
            }}
          >
            <p>label</p>
          </div>
          <Image
            style={{ borderRadius: 12, objectFit: "cover" }}
            src="/images/sample1.png"
            alt="My Image"
            fill
            sizes="400px"
            // objectFit="cover"
          />
        </div>
        <div
          style={{
            textAlign: "center",
            backgroundColor: "aliceblue",
            margin: "0 6px",
            position: "relative",
            height: 115,
            cursor: "pointer",
            overflow: "hidden",
          }}
        >
          <div
            style={{
              position: "absolute",
              bottom: 0,
              left: 0,
              right: 0,
              background:
                "linear-gradient(rgb(255 255 255 / 0%), rgb(81 81 81))",
              zIndex: 1,
              color: "white",
              padding: "12px 0px",
              borderBottomLeftRadius: 12,
              borderBottomRightRadius: 12,
            }}
          >
            <p>label</p>
          </div>
          <Image
            style={{ borderRadius: 12, objectFit: "cover" }}
            src="/images/sample1.png"
            alt="My Image"
            fill
            sizes="400px"
            // objectFit="cover"
          />
        </div>
      </div>
    </div>
  );
};

export default Tourtype;
