"use client";
import Image from "next/image";
import {
  MdStar,
  MdFavorite,
  MdLocationOn,
  MdFavoriteBorder,
} from "react-icons/md";
import { LuAlarmClock } from "react-icons/lu";
import { IoIosArrowRoundForward } from "react-icons/io";
const data = [
  { packageName: "Bali Five Day Lempuyang", pathImg: "/images/sample2.png" },
  { packageName: "Bali Ubud Tour Package", pathImg: "/images/sample3.png" },
  {
    packageName: "Bali Kintamani Tour Package",
    pathImg: "/images/sample4.png",
  },
  { packageName: "Bali Ubud Tour Package", pathImg: "/images/sample3.png" },
  { packageName: "Bali Ubud Tour Package", pathImg: "/images/sample3.png" },
  { packageName: "Bali Ubud Tour Package", pathImg: "/images/sample3.png" },
  { packageName: "Bali Ubud Tour Package", pathImg: "/images/sample3.png" },
  { packageName: "Bali Uluwatu Tour Package", pathImg: "/images/sample1.png" },
  { packageName: "aksjdhaks", pathImg: "/images/sample2.png" },
  { packageName: "aksjdhaks", pathImg: "/images/sample3.png" },
  { packageName: "aksjdhaks", pathImg: "/images/sample2.png" },
  { packageName: "aksjdhaks", pathImg: "/images/sample1.png" },
];

const TourPackage = () => {
  return (
    <div
      style={{
        margin: "0 auto",
        width: "85%",
        marginTop: 72,
        textAlign: "left",
      }}
    >
      <h2 style={{ marginBottom: 8 }}>TOUR PACKAGE</h2>
      <p style={{ marginBottom: 42 }}>
        Getss Bali Tour Packages Prices at affordable rates. Our company Bali
        Tours provide the best facilities for Bali Tour passengers. Enjoy fun
        travel services from Bali Tours.
      </p>
      <div
        style={{
          display: "grid",
          gridTemplateColumns: "repeat(auto-fill, minmax(200px, 2fr))",
          gridGap: 16,
        }}
      >
        {data.map((d, i) => {
          return (
            <div
              key={i}
              style={{
                backgroundColor: "white",
                borderRadius: 8,
              }}
            >
              <div style={{ height: 220, position: "relative" }}>
                <div
                  style={{
                    position: "absolute",
                    top: 16,
                    left: 0,
                    backgroundColor: "white",
                    zIndex: 1,
                    padding: "4px 8px",
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <MdStar color="#FFAF12" />
                  <span style={{ fontSize: 12, marginLeft: 2 }}>5.0</span>
                </div>
                <div
                  style={{
                    position: "absolute",
                    top: 16,
                    right: 16,
                    backgroundColor: "white",
                    zIndex: 1,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    borderRadius: 25,
                    height: 25,
                    width: 25,
                    cursor: "pointer",
                  }}
                >
                  <MdFavorite color="#F80000" size={19} />
                  {/* <MdFavoriteBorder size={19} /> */}
                </div>
                <div
                  style={{
                    position: "absolute",
                    bottom: 0,
                    left: 0,
                    zIndex: 1,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    width: "100%",
                  }}
                >
                  <div
                    style={{
                      width: "100%",
                      background:
                        "linear-gradient(rgb(255 255 255 / 0%), rgb(81 81 81))",
                      color: "white",
                      paddingTop: 6,
                      paddingBottom: 6,
                      paddingLeft: 6,
                      textAlign: "left",
                      fontSize: 12,
                    }}
                  >
                    <MdLocationOn />{" "}
                    <span style={{ fontWeight: "300" }}>Jakarta Selatan</span>
                  </div>
                </div>
                <Image
                  style={{
                    borderTopLeftRadius: 8,
                    borderTopRightRadius: 8,
                    objectFit: "cover",
                  }}
                  src={d.pathImg}
                  alt="My Image"
                  fill
                  sizes="200px"
                />
              </div>
              <div
                style={{
                  padding: 8,
                  display: "flex",
                  flexDirection: "column",
                  fontSize: 13,
                  alignItems: "flex-start",
                }}
              >
                <p style={{ marginBottom: 8, fontWeight: "500" }}>
                  Bali Five Day Lempuyang{" "}
                </p>
                <p style={{ marginBottom: 8, fontWeight: "bold" }}>
                  $1440.00
                  <span style={{ fontWeight: 300, marginLeft: 6 }}>
                    Price starts from
                  </span>
                </p>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    marginBottom: 8,
                  }}
                >
                  <LuAlarmClock />
                  <p style={{ marginLeft: 2 }}>5 Days - 3 Night</p>
                </div>
                <div
                  style={{
                    display: "flex",
                    marginBottom: 12,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <div
                    style={{
                      height: 25,
                      width: 25,
                      borderRadius: 25,
                      position: "relative",
                      marginRight: 6,
                    }}
                  >
                    <Image
                      style={{
                        borderRadius: 25,
                        objectFit: "cover",
                      }}
                      src={d.pathImg}
                      alt="My Image"
                      fill
                      sizes="0"
                    />
                  </div>
                  <p style={{ marginBottom: 6, fontSize: 13 }}>Jhon Doe</p>
                </div>

                <a
                  style={{
                    backgroundColor: "#FF6B00",
                    color: "white",
                    width: "100%",
                    padding: "8px 12px",
                    borderRadius: 6,
                    fontSize: 13,
                  }}
                  href="http://"
                >
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    Explore More <IoIosArrowRoundForward size={20} />
                  </div>
                </a>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default TourPackage;
