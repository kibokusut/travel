"use client";
import Image from "next/image";
import { GoSearch } from "react-icons/go";
import Link from "next/link";

const Header = () => {
  return (
    <header style={{ marginTop: "-66px" }}>
      <div style={{ height: 579, position: "relative" }}>
        <Image
          style={{ objectFit: "cover", width: "100%", height: "100%" }}
          src="/images/sample1.png"
          alt="My Image"
          fill
          sizes="400px"
        />
      </div>

      <div
        style={{
          backgroundColor: "white",
          margin: "0 auto",
          width: "85%",
          marginTop: "-35px",
          position: "absolute",
          left: 0,
          right: 0,
          padding: 12,
          borderRadius: 8,
          display: "flex",
          alignItems: "center",
          paddingLeft: 24,
          paddingRight: 24,
        }}
      >
        <GoSearch size={24} />
        <input
          style={{
            width: "100%",
            padding: 12,
            outline: "none",
            border: "none",
          }}
          type="text"
          placeholder="Find Destination"
        />
        <Link
          style={{
            backgroundColor: "#FF6B00",
            color: "white",
            padding: "12px 24px",
            borderRadius: 6,
          }}
          href="/search"
          // as={"/product/121212"}
          // passHref
        >
          Find
        </Link>
      </div>
    </header>
  );
};

export default Header;
