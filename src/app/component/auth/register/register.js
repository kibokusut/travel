"use client";

import { disableScroll, enableScroll } from "@/utils/scrollock";
import { useEffect, useState } from "react";
import { AiFillCloseCircle } from "react-icons/ai";
import { FcGoogle } from "react-icons/fc";

const Register = ({ islogin, closeModal }) => {
  const [isOpen, setIsOpen] = useState(islogin);

  useEffect(() => {
    if (isOpen) {
      disableScroll(); // Menonaktifkan scroll saat modal terbuka
    } else {
      enableScroll(); // Mengaktifkan kembali scroll saat modal ditutup
    }

    return () => {
      enableScroll(); // Pastikan scroll diaktifkan kembali saat komponen di-unmount
    };
  }, [isOpen]);

  return (
    <div
      style={{
        position: "fixed",
        width: "100%",
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        background: "#00000091",
        zIndex: 12,
      }}
    >
      <div
        style={{
          background: "white",
          padding: "28px 40px 42px 42px",
          width: 450,
          borderRadius: 12,
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "flex-end",
            width: "100%",
            justifyContent: "flex-end",
          }}
        >
          <AiFillCloseCircle
            style={{ cursor: "pointer" }}
            size={"28"}
            onClick={closeModal}
          />
          {/* <button >Close</button> */}
        </div>
        <h1 style={{ marginBottom: 16, marginTop: 16 }}>Sign up</h1>
        <div
          style={{ display: "flex", flexDirection: "column", marginBottom: 24 }}
        >
          <label style={{ fontSize: 14, marginBottom: 8 }} htmlFor="">
            Name *
          </label>
          <input
            style={{
              padding: "12px 16px",
              border: "1px solid #D0D5DD",
              outline: "none",
              backgroundColor: "white",
              borderRadius: 8,
            }}
            type="text"
            placeholder="Name"
          />
        </div>
        <div
          style={{ display: "flex", flexDirection: "column", marginBottom: 24 }}
        >
          <label style={{ fontSize: 14, marginBottom: 8 }} htmlFor="">
            Email or Phone Number *
          </label>
          <input
            style={{
              padding: "12px 16px",
              border: "1px solid #D0D5DD",
              outline: "none",
              backgroundColor: "white",
              borderRadius: 8,
            }}
            type="text"
            placeholder="Email or Phone Number"
          />
        </div>
        <div
          style={{ display: "flex", flexDirection: "column", marginBottom: 24 }}
        >
          <label style={{ fontSize: 14, marginBottom: 8 }} htmlFor="">
            Password *
          </label>
          <input
            style={{
              padding: "12px 16px",
              border: "1px solid #D0D5DD",
              outline: "none",
              backgroundColor: "white",
              borderRadius: 8,
            }}
            type="password"
            placeholder="Password"
          />
          <p style={{ fontSize: 12, paddingTop: 4 }}>
            Must be at least 8 characters.
          </p>
        </div>

        <div>
          <button
            style={{
              backgroundColor: "#FF6B00",
              color: "white",
              width: "100%",
              outline: "none",
              border: "none",
              padding: "12px 14px",
              borderRadius: 8,
              cursor: "pointer",
              marginBottom: 12,
              fontSize: 13,
            }}
          >
            Create account
          </button>
        </div>
        <div
          style={{
            textAlign: "center",
            border: "1px solid #D0D5DD",
            outline: "none",
            backgroundColor: "white",
            borderRadius: 8,
            width: "100%",
            padding: "12px 14px",
            cursor: "pointer",
            marginBottom: 24,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            fontSize: 13,
          }}
        >
          <FcGoogle style={{ marginRight: 12 }} size={20} />
          Sign up with Google
        </div>
        <div style={{ textAlign: "center" }}>
          <label style={{ fontSize: 13 }} htmlFor="">
            Already have an account?
          </label>{" "}
          <a style={{ color: "#FF6B00", fontSize: 13 }} href="http://">
            Log in
          </a>
        </div>
      </div>
    </div>
  );
};

export default Register;
