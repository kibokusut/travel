"use client";
import { Fragment, useEffect, useState } from "react";
import Login from "../auth/login/login";
import Register from "../auth/register/register";
import Image from "next/image";
import { IoIosArrowDown } from "react-icons/io";
import { usePathname } from "next/navigation";
import Link from "next/link";

const Topbar = () => {
  const pageFullUrl = usePathname();
  const [color, setColor] = useState("transparent");
  const [colorTopbar, setColorTopbar] = useState("white");
  const [showLogin, SetShowLogin] = useState(false);
  const [showRegister, SetShowRegister] = useState(false);

  const handleShowModal = () => {
    SetShowLogin(!showLogin);
    SetShowRegister(false);
  };
  const handleShowModalRegister = () => {
    SetShowRegister(!showRegister);
    SetShowLogin(false);
  };

  const closeModal = () => {
    SetShowLogin(false);
    SetShowRegister(false);
  };
  const handleScroll = () => {
    if (window.scrollY > 100) {
      setColor("white");
      setColorTopbar("black");
    } else {
      setColor("transparent");
      setColorTopbar("white");
    }
  };
  useEffect(() => {
    if (pageFullUrl !== "/") {
      setColor("white");
      setColorTopbar("black");
    } else {
      window.addEventListener("scroll", handleScroll);
    }

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [pageFullUrl]);

  return (
    <Fragment>
      {showLogin ? <Login islogin={showLogin} closeModal={closeModal} /> : null}
      {showRegister ? (
        <Register islogin={showRegister} closeModal={closeModal} />
      ) : null}
      <div
        style={{
          width: "100%",
          backgroundColor: color,
          position: "sticky",
          top: 0,
          zIndex: 10,
          alignItems: "center",
          justifyContent: "space-between",
          display: "flex",
          color: colorTopbar,
          paddingLeft: 32,
          paddingRight: 32,
          paddingTop: 8,
          paddingBottom: 8,
        }}
      >
        <div>
          <Link href={"/"}>
            <h1>LOGO</h1>
          </Link>
        </div>
        <div>
          <ul
            style={{
              display: "flex",
              alignItems: "center",
              listStyleType: "none",
            }}
          >
            <li style={{ padding: "6px 12px 6px 12px", fontSize: 14 }}>
              <a href="http://">Pesanan</a>
            </li>
            <li style={{ padding: "6px 12px 6px 12px", fontSize: 14 }}>
              <a href="http://">Daftar Mitra</a>
            </li>
            <li style={{ padding: "6px 12px 6px 12px", fontSize: 14 }}>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  cursor: "pointer",
                }}
              >
                <div
                  style={{
                    height: 25,
                    width: 25,
                    borderRadius: 25,
                    position: "relative",
                    marginRight: 6,
                  }}
                >
                  <Image
                    style={{
                      borderRadius: 25,
                      objectFit: "cover",
                    }}
                    src={"/images/sample2.png"}
                    alt="My Image"
                    fill
                    sizes="0"
                  />
                </div>
                <p style={{ fontSize: 13, marginRight: 3 }}>Jhon Doe</p>
                <IoIosArrowDown />
              </div>
            </li>
            <li
              onClick={handleShowModal}
              style={{
                background: "white",
                color: "black",
                padding: "6px 12px 6px 12px",
                borderRadius: 4,
                cursor: "pointer",
                marginRight: 12,
                fontSize: 14,
                border: "1px solid #D0D5DD",
                marginLeft: 12,
              }}
            >
              Login
            </li>
            <li
              onClick={handleShowModalRegister}
              style={{
                padding: "6px 12px 6px 12px",
                background: "#FF6B00",
                borderRadius: 4,
                color: "white",
                cursor: "pointer",
                fontSize: 14,
              }}
            >
              Register
            </li>
          </ul>
        </div>
      </div>
    </Fragment>
  );
};

export default Topbar;
